#include "Surf.h"


Surf::Surf(const GLchar* vertexShaderPath, const GLchar* fragmentShaderPath)
{
	/* Configure settings and create window */
	settings = new sf::ContextSettings();
    settings->depthBits = 24;
    settings->stencilBits = 8;
    window = new sf::Window(sf::VideoMode(1000, 1000, 32), "OpenGL", sf::Style::Titlebar | sf::Style::Close, *settings);

    /* Initialize GLEW */
    glewExperimental = GL_TRUE;
    glewInit();

    /* Create shader program*/
    vertexShaderFilePath = vertexShaderPath;
    fragmentShaderFilePath = fragmentShaderPath;
    shaderProgram = new Shader(vertexShaderFilePath, fragmentShaderFilePath);

    /* Create camera */
    camera = new Camera(glm::vec3(0.0, 0.0, 2.0), glm::vec3(0.0, 0.0, 0.0), glm::vec3(0.0, 1.0, 0.0));

    /* Initialise matrices */
    model = glm::mat4(1.0f);
    view = glm::mat4(1.0f);
	projection = glm::perspective(45.0f, 1.0f * 640 / 480, 0.1f, 100.0f);
	texture_transform = glm::translate(glm::scale(glm::mat4(1.0f), glm::vec3(1, 1, 1)), glm::vec3(0, 0, 0));

	/* Fill vertices and indices arrays */
	for (int i = 0; i < 201; i++) {
		for (int j = 0; j < 201; j++) {
			vertices[i][j].x = (j - 100) / 100.0;
			vertices[i][j].y = (i - 100) / 100.0;
		}
	}

	int i = 0;
	for (int y = 0; y < 201; y++) {
		for (int x = 0; x < 200; x++) {
			indices[i++] = y * 201 + x;
			indices[i++] = y * 201 + x + 1;
		}
	}

	for (int x = 0; x < 201; x++) {
		for (int y = 0; y < 200; y++) {
			indices[i++] = y * 201 + x;
			indices[i++] = (y + 1) * 201 + x;
		}
	}
}



Surf::Surf(const Surf &surface)
{
	/* Configure settings and create window */
	settings = new sf::ContextSettings();
	settings->depthBits = 24;
    settings->stencilBits = 8;
    window = new sf::Window(sf::VideoMode(1000, 1000, 32), "OpenGL", sf::Style::Titlebar | sf::Style::Close, *settings);

    /* Create shader program*/
	vertexShaderFilePath = surface.vertexShaderFilePath;
    fragmentShaderFilePath = surface.fragmentShaderFilePath;
    shaderProgram = new Shader(vertexShaderFilePath, fragmentShaderFilePath);

    /* Create camera */
    camera = new Camera(glm::vec3(0.0, 0.0, 2.0), glm::vec3(0.0, 0.0, 0.0), glm::vec3(0.0, 1.0, 0.0));

    /* Initialise matrices */
    model = glm::mat4(1.0f);
	view = glm::mat4(1.0f);
	projection = glm::perspective(45.0f, 1.0f * 640 / 480, 0.1f, 100.0f);
	texture_transform = glm::translate(glm::scale(glm::mat4(1.0f), glm::vec3(1, 1, 1)), glm::vec3(0, 0, 0));

	/* Fill vertices and indices arrays */
	for (int i = 0; i < 201; i++) {
		for (int j = 0; j < 201; j++) {
			vertices[i][j].x = (j - 100) / 100.0;
			vertices[i][j].y = (i - 100) / 100.0;
		}
	}

	int i = 0;
	for (int y = 0; y < 201; y++) {
		for (int x = 0; x < 200; x++) {
			indices[i++] = y * 201 + x;
			indices[i++] = y * 201 + x + 1;
		}
	}

	for (int x = 0; x < 201; x++) {
		for (int y = 0; y < 200; y++) {
			indices[i++] = y * 201 + x;
			indices[i++] = (y + 1) * 201 + x;
		}
	}
}



Surf& Surf::operator=(const Surf &surface)
{
	if(this != &surface){
		/* Delete old surface*/
		delete settings;
		delete window;
		delete shaderProgram;
		delete camera;

		/* Configure new settings and create new window */
		settings = new sf::ContextSettings();
		settings->depthBits = 24;
    	settings->stencilBits = 8;
    	window = new sf::Window(sf::VideoMode(1000, 1000, 32), "OpenGL", sf::Style::Titlebar | sf::Style::Close, *settings);

    	/* Create new shader program*/
		vertexShaderFilePath = surface.vertexShaderFilePath;
	    fragmentShaderFilePath = surface.fragmentShaderFilePath;
   	 	shaderProgram = new Shader(vertexShaderFilePath, fragmentShaderFilePath);

   	 	/* Create new camera */
    	camera = new Camera(glm::vec3(0.0, 0.0, 2.0), glm::vec3(0.0, 0.0, 0.0), glm::vec3(0.0, 1.0, 0.0));

    	/* Initialise matrices */
   	 	model = surface.model;
		view = surface.view; 
		projection = surface.projection; 
		texture_transform = surface.texture_transform;

		/* Fill vertices and indices arrays */
		for (int i = 0; i < 201; i++) {
			for (int j = 0; j < 201; j++) {
				vertices[i][j].x = (j - 100) / 100.0;
				vertices[i][j].y = (i - 100) / 100.0;
			}
		}

		int i = 0;
		for (int y = 0; y < 201; y++) {
			for (int x = 0; x < 200; x++) {
				indices[i++] = y * 201 + x;
				indices[i++] = y * 201 + x + 1;
			}
		}

		for (int x = 0; x < 201; x++) {
			for (int y = 0; y < 200; y++) {
				indices[i++] = y * 201 + x;
				indices[i++] = (y + 1) * 201 + x;
			}
		}
	}

	return *this;
}
		


Surf::~Surf()
{
	delete settings;
	delete window;
	delete shaderProgram;
	delete camera;
}







void Surf::draw(float *data, int m, int n, float min, float max)
{
	/* Clear screen to black */
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glClear(GL_COLOR_BUFFER_BIT);

	/* Scale data matrix to be between 0 and 1 where 0 corresponds to min and 1 corresponds to max */
	scale(data, m, n, min, max);

	/* Update matrices for user input */
	processEvents();

	/* Upload the texture with our data points */
	GLuint texture_id;
	glActiveTexture(GL_TEXTURE0);
	glGenTextures(1, &texture_id);
	glBindTexture(GL_TEXTURE_2D, texture_id);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, m, n, 0, GL_RED, GL_FLOAT, data);

	/* Link matrices with shader program */
	shaderProgram->use();
	GLint attribute_coord2d = shaderProgram->getAttrib("coord2d");
	GLint modelLoc = shaderProgram->getUniform("model");
	GLint viewLoc = shaderProgram->getUniform("view");
	GLint projLoc = shaderProgram->getUniform("projection");
	GLint uniform_texture_transform = shaderProgram->getUniform("texture_transform");
	GLint uniform_mytexture = shaderProgram->getUniform("mytexture");
	glUniform1i(uniform_mytexture, 0);
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
	glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));
	glUniformMatrix4fv(uniform_texture_transform, 1, GL_FALSE, glm::value_ptr(texture_transform));

	/* Set texture wrapping mode */
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	/* Set texture interpolation mode */
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	// Create two vertex buffer objects
	glGenBuffers(2, vbo);

	// Tell OpenGL to copy our vertices and indices arrays to buffer objects
	glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof vertices, vertices, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo[1]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof indices, indices, GL_DYNAMIC_DRAW);

	/* Draw the grid using the indices to our vertices using our vertex buffer objects */
	glEnableVertexAttribArray(attribute_coord2d);
	glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
	glVertexAttribPointer(attribute_coord2d, 2, GL_FLOAT, GL_FALSE, 0, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo[1]);
	glDrawElements(GL_LINES, 200 * 201 * 4, GL_UNSIGNED_SHORT, 0);

	/* Stop using the vertex buffer object */
	glDisableVertexAttribArray(attribute_coord2d);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	/* Free buffers */
	glDeleteBuffers(2, vbo);

	window->display();
}



void Surf::processEvents()
{
	sf::Event event;
	while(window->pollEvent(event)){
		if(event.type == sf::Event::MouseWheelMoved){
			camera->processMouseScrollInput(event.mouseWheel.delta);
		}
		else if(event.type == sf::Event::MouseButtonPressed){
			std::cout << "left mouse button pressed" << std::endl;
			sf::Vector2i mpos = sf::Mouse::getPosition();
			camera->mouseButtonPressed(mpos.x, mpos.y);
		}
		else if(event.type == sf::Event::MouseButtonReleased){
			std::cout << "left mouse button released" << std::endl;
			camera->mouseButtonReleased();
		}
		else if(event.type == sf::Event::Closed){
			window->close();
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)){
			camera->processKeyboardInput(LEFT);
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)){
			camera->processKeyboardInput(RIGHT);
		}
		else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up)){
			camera->processKeyboardInput(UP);
		}
		else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down)){
			camera->processKeyboardInput(DOWN);
		}
		else if(sf::Mouse::isButtonPressed(sf::Mouse::Left)){
			sf::Vector2i mpos = sf::Mouse::getPosition();
			camera->processMouseButtonInput(mpos.x, mpos.y);
			std::cout<<"mouse button is being pressed down"<<std::endl;
		}
	}

	view = camera->getViewMatrix();
}


void Surf::scale(float *data, int m, int n, float min, float max)
{
	for(int i=0;i<m*n;i++){
		data[i] = (data[i] - min) / (max - min);
	}
}