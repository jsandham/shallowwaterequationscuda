#ifndef __KERNELS_CUH__
#define __KERNELS_CUH__

__device__ float LaxFriedrichFlux(float low1, float high1, float low2, float high2, float delta);

__global__ void apply_boundary_conditions_kernel(float *h, float *hu, float *hv, int nx);
__global__ void compute_horizontal_flux_kernel(float *Fh, float *Fhu, float *Fhv, float *h, float *hu, float *hv, int nx);
__global__ void compute_vertical_flux_kernel(float *Gh, float *Ghu, float *Ghv, float *h, float *hu, float *hv, int nx);
__global__ void compute_horizontal_edge_flux_kernel(float *Fh, float *Fhu, float *Fhv, float *h, float *hu, float *hv, int nx);
__global__ void compute_vertical_edge_flux_kernel(float *Gh, float *Ghu, float *Ghv, float *h, float *hu, float *hv, int nx);
__global__ void update_height_kernel(float *h, float *Fh, float *Gh, int nx);
__global__ void update_horizontal_momentum_kernel(float *hu, float *Fhu, float *Ghu, int nx);
__global__ void update_vertical_momentum_kernel(float *hv, float *Fhv, float *Ghv, int nx);
__global__ void compute_maximum_timestep_kernel(int *mutex, float *h, float *hu, float *hv, float *hmax, float *vmax, float *vc, int nx, int ny);


__global__ void compute_horizontal_flux_kernel2(float *Fh, float *Fhu, float *Fhv, float *h, float *hu, float *hv, int nx, int ny);
__global__ void compute_maximum_timestep_kernel2(int *mutex, float *h, float *hu, float *hv, float *hmax, float *vmax, float *vc, int nx);



#endif
