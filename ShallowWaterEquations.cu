#include <iostream>
#include "ShallowWaterEquations.h"
#include "kernels.cuh"


#include <stdio.h>
// #include <cuda.h>
// #include <cuda_runtime.h>

// ==========================================================================================
// CUDA ERROR CHECKING CODE
#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess)
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) getchar();
   }
}

// ==========================================================================================



ShallowWaterEquations::ShallowWaterEquations(SimulationParameters parameters, int nx, int ny) : parameters(parameters), nx(nx), ny(ny)
{
	h_h = new float[(nx+2)*(ny+2)];
	h_hu = new float[(nx+2)*(ny+2)];
	h_hv = new float[(nx+2)*(ny+2)];
	h_Fh = new float[(nx+1)*(ny+1)]; 
	h_Fhu = new float[(nx+1)*(ny+1)]; 
	h_Fhv = new float[(nx+1)*(ny+1)]; 
	h_Gh = new float[(nx+1)*(ny+1)]; 
	h_Ghu = new float[(nx+1)*(ny+1)]; 
	h_Ghv = new float[(nx+1)*(ny+1)];
	h_hmax = new float;
	h_vmax = new float;
	h_vc = new float;

	gpuErrchk(cudaMalloc((void**)&d_h, (nx+2)*(ny+2)*sizeof(float)));
	gpuErrchk(cudaMalloc((void**)&d_hu, (nx+2)*(ny+2)*sizeof(float)));
	gpuErrchk(cudaMalloc((void**)&d_hv, (nx+2)*(ny+2)*sizeof(float)));
	gpuErrchk(cudaMalloc((void**)&d_Fh, (nx+1)*(ny+1)*sizeof(float)));
	gpuErrchk(cudaMalloc((void**)&d_Fhu, (nx+1)*(ny+1)*sizeof(float)));
	gpuErrchk(cudaMalloc((void**)&d_Fhv, (nx+1)*(ny+1)*sizeof(float)));
	gpuErrchk(cudaMalloc((void**)&d_Gh, (nx+1)*(ny+1)*sizeof(float)));
	gpuErrchk(cudaMalloc((void**)&d_Ghu, (nx+1)*(ny+1)*sizeof(float)));
	gpuErrchk(cudaMalloc((void**)&d_Ghv, (nx+1)*(ny+1)*sizeof(float)));
	gpuErrchk(cudaMalloc((void**)&d_hmax, sizeof(float)));
	gpuErrchk(cudaMalloc((void**)&d_vmax, sizeof(float)));
	gpuErrchk(cudaMalloc((void**)&d_vc, sizeof(float)));
	gpuErrchk(cudaMalloc((void**)&d_mutex, sizeof(int)));

	gpuErrchk(cudaMemset(d_h, 0, (nx+2)*(ny+2)*sizeof(float)));
	gpuErrchk(cudaMemset(d_hu, 0, (nx+2)*(ny+2)*sizeof(float)));
	gpuErrchk(cudaMemset(d_hv, 0, (nx+2)*(ny+2)*sizeof(float)));
	gpuErrchk(cudaMemset(d_Fh, 0, (nx+1)*(ny+1)*sizeof(float)));
	gpuErrchk(cudaMemset(d_Gh, 0, (nx+1)*(ny+1)*sizeof(float)));
	gpuErrchk(cudaMemset(d_hmax, 0, sizeof(float)));
	gpuErrchk(cudaMemset(d_vmax, 0, sizeof(float)));
	gpuErrchk(cudaMemset(d_vc, 0.001f, sizeof(float)));

	display = new Surf("vertex.vs", "fragment.frag");
}


ShallowWaterEquations::ShallowWaterEquations(const ShallowWaterEquations &swe)
{
	parameters = swe.parameters;
	nx = swe.nx;
	ny = swe.ny;
	h_h = new float[(nx+2)*(ny+2)];
	h_hu = new float[(nx+2)*(ny+2)];
	h_hv = new float[(nx+2)*(ny+2)];
	h_Fh = new float[(nx+1)*(ny+1)]; 
	h_Fhu = new float[(nx+1)*(ny+1)]; 
	h_Fhv = new float[(nx+1)*(ny+1)]; 
	h_Gh = new float[(nx+1)*(ny+1)]; 
	h_Ghu = new float[(nx+1)*(ny+1)]; 
	h_Ghv = new float[(nx+1)*(ny+1)];
	h_hmax = new float;
	h_vmax = new float;
	h_vc = new float;

	gpuErrchk(cudaMalloc((void**)&d_h, (nx+2)*(ny+2)*sizeof(float)));
	gpuErrchk(cudaMalloc((void**)&d_hu, (nx+2)*(ny+2)*sizeof(float)));
	gpuErrchk(cudaMalloc((void**)&d_hv, (nx+2)*(ny+2)*sizeof(float)));
	gpuErrchk(cudaMalloc((void**)&d_Fh, (nx+1)*(ny+1)*sizeof(float)));
	gpuErrchk(cudaMalloc((void**)&d_Fhu, (nx+1)*(ny+1)*sizeof(float)));
	gpuErrchk(cudaMalloc((void**)&d_Fhv, (nx+1)*(ny+1)*sizeof(float)));
	gpuErrchk(cudaMalloc((void**)&d_Gh, (nx+1)*(ny+1)*sizeof(float)));
	gpuErrchk(cudaMalloc((void**)&d_Ghu, (nx+1)*(ny+1)*sizeof(float)));
	gpuErrchk(cudaMalloc((void**)&d_Ghv, (nx+1)*(ny+1)*sizeof(float)));
	gpuErrchk(cudaMalloc((void**)&d_hmax, sizeof(float)));
	gpuErrchk(cudaMalloc((void**)&d_vmax, sizeof(float)));
	gpuErrchk(cudaMalloc((void**)&d_vc, sizeof(float)));
	gpuErrchk(cudaMalloc((void**)&d_mutex, sizeof(int)));

	gpuErrchk(cudaMemset(d_h, 0, (nx+2)*(ny+2)*sizeof(float)));
	gpuErrchk(cudaMemset(d_hu, 0, (nx+2)*(ny+2)*sizeof(float)));
	gpuErrchk(cudaMemset(d_hv, 0, (nx+2)*(ny+2)*sizeof(float)));
	gpuErrchk(cudaMemset(d_Fh, 0, (nx+1)*(ny+1)*sizeof(float)));
	gpuErrchk(cudaMemset(d_Gh, 0, (nx+1)*(ny+1)*sizeof(float)));
	gpuErrchk(cudaMemset(d_hmax, 0, sizeof(float)));
	gpuErrchk(cudaMemset(d_vmax, 0, sizeof(float)));
	gpuErrchk(cudaMemset(d_vc, 0.001f, sizeof(float)));

	display = new Surf("vertex.vs", "fragment.frag");
}


ShallowWaterEquations& ShallowWaterEquations::operator=(const ShallowWaterEquations &swe)
{
	if(this != &swe){
		// delete old object
		delete [] h_h;
		delete [] h_hu;
		delete [] h_hv;
		delete [] h_Fh;
		delete [] h_Fhu;
		delete [] h_Fhv;
		delete [] h_Gh;
		delete [] h_Ghu;
		delete [] h_Ghv;
		delete h_hmax;
		delete h_vmax;
		delete h_vc;

		gpuErrchk(cudaFree(d_h));
		gpuErrchk(cudaFree(d_hu));
		gpuErrchk(cudaFree(d_hv));
		gpuErrchk(cudaFree(d_Fh));
		gpuErrchk(cudaFree(d_Fhu));
		gpuErrchk(cudaFree(d_Fhv));
		gpuErrchk(cudaFree(d_Gh));
		gpuErrchk(cudaFree(d_Ghu));
		gpuErrchk(cudaFree(d_Ghv));
		gpuErrchk(cudaFree(d_hmax));
		gpuErrchk(cudaFree(d_vmax));
		gpuErrchk(cudaFree(d_vc));
		gpuErrchk(cudaFree(d_mutex));

		delete display;

		// create new object
		this->parameters = swe.parameters;
		this->nx = swe.nx;
		this->ny = swe.ny;
		h_h = new float[(nx+2)*(ny+2)];
		h_hu = new float[(nx+2)*(ny+2)];
		h_hv = new float[(nx+2)*(ny+2)];
		h_Fh = new float[(nx+1)*(ny+1)]; 
		h_Fhu = new float[(nx+1)*(ny+1)]; 
		h_Fhv = new float[(nx+1)*(ny+1)]; 
		h_Gh = new float[(nx+1)*(ny+1)]; 
		h_Ghu = new float[(nx+1)*(ny+1)]; 
		h_Ghv = new float[(nx+1)*(ny+1)];
		h_hmax = new float;
		h_vmax = new float;
		h_vc = new float;

		gpuErrchk(cudaMalloc((void**)&d_h, (nx+2)*(ny+2)*sizeof(float)));
		gpuErrchk(cudaMalloc((void**)&d_hu, (nx+2)*(ny+2)*sizeof(float)));
		gpuErrchk(cudaMalloc((void**)&d_hv, (nx+2)*(ny+2)*sizeof(float)));
		gpuErrchk(cudaMalloc((void**)&d_Fh, (nx+1)*(ny+1)*sizeof(float)));
		gpuErrchk(cudaMalloc((void**)&d_Fhu, (nx+1)*(ny+1)*sizeof(float)));
		gpuErrchk(cudaMalloc((void**)&d_Fhv, (nx+1)*(ny+1)*sizeof(float)));
		gpuErrchk(cudaMalloc((void**)&d_Gh, (nx+1)*(ny+1)*sizeof(float)));
		gpuErrchk(cudaMalloc((void**)&d_Ghu, (nx+1)*(ny+1)*sizeof(float)));
		gpuErrchk(cudaMalloc((void**)&d_Ghv, (nx+1)*(ny+1)*sizeof(float)));
		gpuErrchk(cudaMalloc((void**)&d_hmax, sizeof(float)));
		gpuErrchk(cudaMalloc((void**)&d_vmax, sizeof(float)));
		gpuErrchk(cudaMalloc((void**)&d_vc, sizeof(float)));
		gpuErrchk(cudaMalloc((void**)&d_mutex, sizeof(int)));

		gpuErrchk(cudaMemset(d_h, 0, (nx+2)*(ny+2)*sizeof(float)));
		gpuErrchk(cudaMemset(d_hu, 0, (nx+2)*(ny+2)*sizeof(float)));
		gpuErrchk(cudaMemset(d_hv, 0, (nx+2)*(ny+2)*sizeof(float)));
		gpuErrchk(cudaMemset(d_Fh, 0, (nx+1)*(ny+1)*sizeof(float)));
		gpuErrchk(cudaMemset(d_Gh, 0, (nx+1)*(ny+1)*sizeof(float)));
		gpuErrchk(cudaMemset(d_hmax, 0, sizeof(float)));
		gpuErrchk(cudaMemset(d_vmax, 0, sizeof(float)));
		gpuErrchk(cudaMemset(d_vc, 0.001f, sizeof(float)));

		display = new Surf("vertex.vs", "fragment.frag");
	}

	return *this;
}


ShallowWaterEquations::~ShallowWaterEquations()
{
	delete [] h_h;
	delete [] h_hu;
	delete [] h_hv;
	delete [] h_Fh;
	delete [] h_Fhu;
	delete [] h_Fhv;
	delete [] h_Gh;
	delete [] h_Ghu;
	delete [] h_Ghv;
	delete h_hmax;
	delete h_vmax;
	delete h_vc;

	gpuErrchk(cudaFree(d_h));
	gpuErrchk(cudaFree(d_hu));
	gpuErrchk(cudaFree(d_hv));
	gpuErrchk(cudaFree(d_Fh));
	gpuErrchk(cudaFree(d_Fhu));
	gpuErrchk(cudaFree(d_Fhv));
	gpuErrchk(cudaFree(d_Gh));
	gpuErrchk(cudaFree(d_Ghu));
	gpuErrchk(cudaFree(d_Ghv));
	gpuErrchk(cudaFree(d_hmax));
	gpuErrchk(cudaFree(d_vmax));
	gpuErrchk(cudaFree(d_vc));
	gpuErrchk(cudaFree(d_mutex));

	delete display;
}


void ShallowWaterEquations::runSimulation()
{
	int t=0;
	while(t<parameters.iterations){
		float elapsedTime;
		cudaEventCreate(&start);
		cudaEventCreate(&stop);
		cudaEventRecord(start,0);

		setBoundaryConditions();
		computeFluxes();
		getMaxTimestep();
		update();

		cudaEventRecord(stop,0);
		cudaEventSynchronize(stop);
		cudaEventElapsedTime(&elapsedTime, start, stop);
		cudaEventDestroy(start);
		cudaEventDestroy(stop);

		if(parameters.benchmark){
			std::cout<<"Timestep: "<<t<<"    "<<"Elapsed time: "<<elapsedTime<<std::endl;
		}

		if(parameters.opengl){
			// copy height array back to host
			cudaMemcpy(h_h, d_h, (nx+2)*(ny+2)*sizeof(float), cudaMemcpyDeviceToHost);
			// cudaMemcpy(h_Fh, d_Fh, (nx+1)*(ny+1)*sizeof(float), cudaMemcpyDeviceToHost);
			// cudaMemcpy(h_Gh, d_Gh, (nx+1)*(ny+1)*sizeof(float), cudaMemcpyDeviceToHost);

			display->draw(h_h, nx+2, ny+2, -4 ,4);
			// display->draw(h_Fh, nx+1, ny+1, -2 ,2);
			// display->draw(h_Gh, nx+1, ny+1, -2 ,2);
		}

		t++;
	}
}


void ShallowWaterEquations::setBoundaryConditions()
{
	// call kernels to set ghost points to proper values by apply boundary conditions
	dim3 blockSize(16, 1);
	dim3 gridSize(64, 1);
	apply_boundary_conditions_kernel<<< gridSize, blockSize >>>(d_h, d_hu, d_hv, nx);
}


void ShallowWaterEquations::computeFluxes()
{
	// call kernels to update d_Fh, d_Fhu, d_Fhv and d_Gh, d_Ghu, d_Ghv
	dim3 blockSize(16, 16);
	dim3 gridSize(64, 64);
	compute_horizontal_flux_kernel<<< gridSize, blockSize >>>(d_Fh, d_Fhu, d_Fhv, d_h, d_hu, d_hv, nx);
	compute_vertical_flux_kernel<<< gridSize, blockSize >>>(d_Gh, d_Ghu, d_Ghv, d_h, d_hu, d_hv, ny);

	dim3 rightBlockSize(1, 16);
	dim3 rightGridSize(1, 64);
	compute_horizontal_edge_flux_kernel<<< rightGridSize, rightBlockSize >>>(d_Fh, d_Fhu, d_Fhv, d_h, d_hu, d_hv, nx);
	
	dim3 bottomBlockSize(16, 1);
	dim3 bottomGridSize(64, 1);
	compute_vertical_edge_flux_kernel<<< bottomGridSize, bottomBlockSize >>>(d_Gh, d_Ghu, d_Ghv, d_h, d_hu, d_hv, ny);
}


void ShallowWaterEquations::getMaxTimestep()
{
	// TO DO: for now just leave timestep as constant
	// gpuErrchk(cudaMemset(d_hmax, 0, sizeof(float)));
	// gpuErrchk(cudaMemset(d_vmax, 0, sizeof(float)));
	// gpuErrchk(cudaMemset(d_mutex, 0, sizeof(float)));

	// dim3 blockSize(256, 1);
	// dim3 gridSize(256, 1);
	// compute_maximum_timestep_kernel<<< gridSize, blockSize >>>(d_mutex, d_h, d_hu, d_hv, d_hmax, d_vmax, d_vc, nx, ny);

	// cudaMemcpy(h_vc, d_vc, sizeof(float), cudaMemcpyDeviceToHost);

	// std::cout << "characteristic velocity: " << *h_vc << std::endl;
	// std::cout << "timestep dt: " << 0.02/(*h_vc) << std::endl;

	// dim3 blockSize(64, 64);
	// dim3 gridSize(64, 64);
	// compute_maximum_timestep_kernel2<<< gridSize, blockSize >>>(d_mutex, d_h, d_hu, d_hv, d_hmax, d_vmax, d_vc, nx);
}


void ShallowWaterEquations::update()
{
	// call kernels to update d_h, d_hu, d_hv
	dim3 blockSize(16, 16);
	dim3 gridSize(64, 64);
	update_height_kernel<<< gridSize, blockSize >>>(d_h, d_Fh, d_Gh, nx);
	update_horizontal_momentum_kernel<<< gridSize, blockSize >>>(d_hu, d_Fhu, d_Ghu, nx);
	update_vertical_momentum_kernel<<< gridSize, blockSize >>>(d_hv, d_Fhv, d_Ghv, nx);
}



void ShallowWaterEquations::initialCondition()
{
	for(int i=0;i<nx+2; i++){
		for(int j=0;j<ny+2;j++){
			if(i > nx/4 && i < 3*nx/4 && j > ny/4 && j < 3*ny/4){
				h_h[i + (nx+2)*j] = 5.5;
			}
			else{
				h_h[i + (nx+2)*j] = 1.0;
			}
		}
	}

	// for(int i=0;i<nx+2; i++){
	// 	for(int j=0;j<ny+2;j++){
	// 		if(i < nx/4){
	// 			h_h[i + (nx+2)*j] = 5.5;
	// 		}
	// 		else{
	// 			h_h[i + (nx+2)*j] = 1.0;
	// 		}
	// 	}
	// }

	// copy arrays to device
	cudaMemcpy(d_h, h_h, (nx+2)*(ny+2)*sizeof(float), cudaMemcpyHostToDevice);
}