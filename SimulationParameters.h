#ifndef __PARAMETERS_H__
#define __PARAMETERS_H__

typedef struct SimulationParameters
{
	float dt;
	float dx;
	float dy;
	bool benchmark;
	bool opengl;
	int iterations;

}SimulationParameters;


#endif