#ifndef __CAMERA_H__
#define __CAMERA_H__

#include <iostream>

#include <GL/gl.h>

#define GLM_FORCE_RADIANS

#include "../../glm/glm/glm.hpp"
#include "../../glm/glm/gtc/matrix_transform.hpp"
#include "../../glm/glm/gtc/type_ptr.hpp"

enum KEY
{
	LEFT,
	RIGHT,
	UP,
	DOWN
};


class Camera
{
	private:
		glm::vec3 position;
		glm::vec3 target;
		glm::vec3 up;
		glm::vec3 front;
		glm::vec3 temp;

		GLfloat pitch;
		GLfloat yaw;

		glm::vec2 lastPos;


	public:
		Camera(glm::vec3 position, glm::vec3 target, glm::vec3 up);

		glm::mat4 getViewMatrix();
		void processKeyboardInput(KEY k);
		void processMouseButtonInput(GLfloat x, GLfloat y);
		void processMouseScrollInput(GLfloat scroll);
		void mouseButtonPressed(GLfloat x, GLfloat y);
		void mouseButtonReleased();

	private:
		void update();

};


#endif