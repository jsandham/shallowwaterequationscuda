#ifndef __SURF_H__
#define __SURF_H__

#include <cstdlib>
#include <iostream>

#define GLEW_STATIC
#include <GL/glew.h>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define GLM_FORCE_RADIANS

#include "../../glm/glm/glm.hpp"
#include "../../glm/glm/gtc/matrix_transform.hpp"
#include "../../glm/glm/gtc/type_ptr.hpp"

#include "Shader.h"
#include "Camera.h"


class Surf
{
	private:
		const GLchar* vertexShaderFilePath;
		const GLchar* fragmentShaderFilePath;

		sf::ContextSettings *settings;
		sf::Window *window;
		Shader *shaderProgram;
		Camera *camera;

		glm::mat4 model;
		glm::mat4 view;
		glm::mat4 projection;
		glm::mat4 texture_transform;

		GLuint vbo[2];
		glm::vec2 vertices[201][201];
		GLushort indices[200 * 201 * 4];


	public:
		Surf(const GLchar* vertexShaderPath, const GLchar* fragmentShaderPath);
		Surf(const Surf &surface);
		Surf& operator=(const Surf &surface);
		~Surf();

		void draw(float *data, int m, int n, float min, float max);


	private:
		void processEvents();
		void scale(float *data, int m, int n, float min, float max);
};



#endif