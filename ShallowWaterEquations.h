#ifndef __SHALLOWWATEREQUATIONS_H__
#define __SHALLOWWATEREQUATIONS_H__

#include <cuda.h>
#include <cuda_runtime.h>

#include "SimulationParameters.h"
#include "Surf.h"


class ShallowWaterEquations
{
	private:
		// simulation parameters
		SimulationParameters parameters;

		// number of points in x and y direction
		int nx, ny;

		// height and momentum variables on host 
		float *h_h, *h_hu, *h_hv; 

		// horizontal and vertical flux variables on host
		float *h_Fh, *h_Fhu, *h_Fhv, *h_Gh, *h_Ghu, *h_Ghv;

		// max height, max velocity, and characteristic velocity on host
		float *h_hmax, *h_vmax, *h_vc;

		// height and momentum variables on device
		float *d_h, *d_hu, *d_hv; 

		// horizontal and vertical flux variables on device
		float *d_Fh, *d_Fhu, *d_Fhv, *d_Gh, *d_Ghu, *d_Ghv;

		// max height, max velocity, and characteristic velocity on device
		float *d_hmax, *d_vmax, *d_vc;

		// used for locking on device
		int *d_mutex;

		// opengl surface display window
		Surf *display;

		// used for timing
		cudaEvent_t start, stop; 

	public:
		ShallowWaterEquations(SimulationParameters parameters, int nx, int ny);
		ShallowWaterEquations(const ShallowWaterEquations &swe);
		ShallowWaterEquations& operator=(const ShallowWaterEquations &swe);
		~ShallowWaterEquations();

		void runSimulation();
		void initialCondition();

	private:
		void setBoundaryConditions();
		void computeFluxes();
		void getMaxTimestep();
		void update();
};



#endif