#include <iostream>
#include "SimulationParameters.h"
#include "ShallowWaterEquations.h"

bool checkCmdLineFlag(const int argc, char** argv, const char* string)
{
	bool flag = false;
	if(argc > 1){
		for(int i=1;i<argc;i++){
			if(strcmp(&argv[i][1], string) == 0){
				flag = true;
				break;
			}
		}
	}

	return flag;
}



int getCmdLineParameterInt(const int argc, char** argv, const char* string, const int defaultValue)
{
	int parameter = defaultValue;
	if(argc > 1){
		for(int i=1;i<argc;i++){
			char* str = &argv[i][1];
			char* eqs = strchr(str, '=');

			if(eqs != NULL){
				int index = (int)(eqs-str);
				char substring[50];
				strncpy(substring, str, index);
				substring[index] = '\0';

				if(strcmp(substring, string)==0){
					parameter = atoi(&eqs[1]);
					break;
				}
			}
		}
	}
	return parameter;
}


float getCmdLineParameterFloat(const int argc, char** argv, const char* string, const float defaultValue)
{
	float parameter = defaultValue;
	if(argc > 1){
		for(int i=1;i<argc;i++){
			char* str = &argv[i][1];
			char* eqs = strchr(str, '=');

			if(eqs != NULL){
				int index = (int)(eqs-str);
				char substring[50];
				strncpy(substring, str, index);
				substring[index] = '\0';

				if(strcmp(substring, string)==0){
					parameter = atof(&eqs[1]);
					break;
				}
			}

		}
	}
	return parameter;
}



int main(int argc, char **argv)
{
	SimulationParameters parameters;
	parameters.dx = 0.02;
	parameters.dy = 0.02;
	parameters.dy = 0.001;
	parameters.benchmark = checkCmdLineFlag(argc, argv, "benchmark");
	parameters.opengl = checkCmdLineFlag(argc, argv, "opengl");
	parameters.iterations = getCmdLineParameterInt(argc, argv, "iterations", 100);

	ShallowWaterEquations swe(parameters, 1024, 1024);

	swe.initialCondition();
	swe.runSimulation();
}
