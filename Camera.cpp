#include "Camera.h"


Camera::Camera(glm::vec3 position, glm::vec3 target, glm::vec3 up)
{
	this->position = position;  // position of camera
	this->target = target;      // target of camera
	this->up = up;			    // up vector for camera
	this->front = position - target;
	this->temp = target;

 	this->yaw = 90.0f;
	this->pitch = 0.0f;
}


glm::mat4 Camera::getViewMatrix()
{
	return glm::lookAt(position, temp, up);
}



void Camera::processKeyboardInput(KEY k)
{
	GLfloat factor = 0.2;
	if(k == LEFT){
		position += factor*glm::normalize(glm::cross(front, up));
	}
	else if (k == RIGHT){
		position -= factor*glm::normalize(glm::cross(front, up));
	}
	else if(k == UP){
		position += factor*glm::normalize(up);
	}
	else if(k == DOWN){
		position -= factor*glm::normalize(up);
	}

	temp = position - front;
	target = position - front;
}



void Camera::processMouseButtonInput(GLfloat x, GLfloat y)
{
	GLfloat offsetx = 0.0;
	GLfloat offsety = 0.0;

	offsetx = x - lastPos.x;
	offsety = y - lastPos.y;
	lastPos.x = x;
	lastPos.y = y;

	yaw += offsetx;
	pitch += offsety;

	GLfloat dx = position.x - target.x;
	GLfloat dy = position.y - target.y;
	GLfloat dz = position.z - target.z;
	GLfloat radius = glm::sqrt(dx*dx + dy*dy + dz*dz);
	position.x = radius*cos(glm::radians(yaw))*cos(glm::radians(pitch)) + target.x;
	position.y = radius*sin(glm::radians(pitch)) + target.y;
	position.z = radius*sin(glm::radians(yaw))*cos(glm::radians(pitch)) + target.z;

	update();

	// temp = target;
    temp = position - front;
}


void Camera::processMouseScrollInput(GLfloat scroll)
{
	GLfloat factor = 0.2;
	position += factor*scroll*front;
}


void Camera::mouseButtonPressed(GLfloat x, GLfloat y)
{
	lastPos.x = x;
	lastPos.y = y;
}



void Camera::mouseButtonReleased()
{

}


void Camera::update()
{
    front = position - target;
    up.x = -cos(glm::radians(yaw))*sin(glm::radians(pitch));
    up.y = cos(glm::radians(pitch));
    up.z = -sin(glm::radians(yaw))*sin(glm::radians(pitch));
}