#Makefile for nbody code

#define variables
objects= main.o Shader.o Camera.o Surf.o kernels.o ShallowWaterEquations.o
NVCC= nvcc              #cuda c compiler
CPP= g++                 #c++ compiler
opt= -O2 -g -G           #optimization flag
ARCH= -arch=sm_30        #cuda compute capability  
LIBS= -lGLU -lGL -lGLEW -lm -lsfml-graphics -lsfml-window -lsfml-system 
execname= main


build: $(objects)
	$(NVCC) $(opt) $(ARCH) -o $(execname) $(objects) $(LIBS)

Shader.o: Shader.cpp
	$(NVCC) $(opt) -c Shader.cpp
Camera.o: Camera.cpp
	$(NVCC) $(opt) -c Camera.cpp
Surf.o: Surf.cpp
	$(NVCC) $(opt) -c Surf.cpp
kernels.o: kernels.cu
	$(NVCC) $(opt) $(ARCH) -c kernels.cu
ShallowWaterEquations.o: ShallowWaterEquations.cu
	$(NVCC) $(opt) $(ARCH) -c ShallowWaterEquations.cu
main.o: main.cu
	$(NVCC) $(opt) $(ARCH) -c main.cu


clean:
	rm $(objects)
