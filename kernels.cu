#include <cuda.h>
#include <cuda_runtime.h>
#include "kernels.cuh"

/*
	-----------> +x
	|
	|
	|
	|
	|
	v
	+y

	 0   1   2   3   4   5   6   7   8   9
	   |   |   |   |   |   |   |   |   |   
0	 * | * | * | * | * | * | * | * | * | * 
	___|___|___|___|___|___|___|___|___|___
	   |   |   |   |   |   |   |   |   |   
1	 * | * | * | * | * | * | * | * | * | * 
	___|___|___|___|___|___|___|___|___|___
	   |   |   |   |   |   |   |   |   |   
2	 * | * | * | * | * | * | * | * | * | * 
	___|___|___|___|___|___|___|___|___|___
	   |   |   |   |   |   |   |   |   |   
3	 * | * | * | * | * | * | * | * | * | * 
	___|___|___|___|___|___|___|___|___|___
	   |   |   |   |   |   |   |   |   |   
4	 * | * | * | * | * | * | * | * | * | * 
	___|___|___|___|___|___|___|___|___|___
	   |   |   |   |   |   |   |   |   |   
5	 * | * | * | * | * | * | * | * | * | * 
	___|___|___|___|___|___|___|___|___|___
	   |   |   |   |   |   |   |   |   |   
6	 * | * | * | * | * | * | * | * | * | * 
	___|___|___|___|___|___|___|___|___|___
	   |   |   |   |   |   |   |   |   |   
7	 * | * | * | * | * | * | * | * | * | * 
	___|___|___|___|___|___|___|___|___|___
	   |   |   |   |   |   |   |   |   |   
8	 * | * | * | * | * | * | * | * | * | * 
	___|___|___|___|___|___|___|___|___|___
	   |   |   |   |   |   |   |   |   |   
9	 * | * | * | * | * | * | * | * | * | * 
	   |   |   |   |   |   |   |   |   |   

*/



__device__ const int blockSize = 256; 
__device__ const float g = 9.80f;
__device__ const float dt = 0.0025f;
__device__ const float dx = 0.02f;
__device__ const float dy = 0.02f;


__device__ float LaxFriedrichFlux(float low1, float high1, float low2, float high2, float delta)
{
	return 0.5f*(low1 + high1) + delta*(low2 - high2);
}




__global__ void apply_boundary_conditions_kernel(float *h, float *hu, float *hv, int nx)
{
	int index = threadIdx.x + blockIdx.x*blockDim.x + 1;

	h[index] = h[index + nx+2];
	h[index + (nx+2)*(nx+1)] = h[index + (nx+2)*nx];
	h[index*(nx+2)] = h[index*(nx+2) + 1];
	h[index*(nx+2) + nx+1] = h[index*(nx+2) + nx];

	hu[index*(nx+2)] = -hu[index*(nx+2) + 1];
	hu[index*(nx+2) + nx+1] = -hu[index*(nx+2) + nx];

	hv[index] = -hv[index + nx+2];
	hv[index + (nx+2)*(nx+1)] = -hv[index + (nx+2)*nx];
}


__global__ void compute_horizontal_flux_kernel(float *Fh, float *Fhu, float *Fhv, float *h, float *hu, float *hv, int nx)
{
	int x = threadIdx.x + blockIdx.x*blockDim.x;
	int y = threadIdx.y + blockIdx.y*blockDim.y;

	int index = x + (nx+1)*y;
	int left = x + (nx + 2)*(y + 1);
	int right = x + 1 + (nx + 2)*(y + 1);

	float factor = fmaxf(fabs(hu[left]/h[left]), fabs(hu[right]/h[right]));
	Fh[index] = LaxFriedrichFlux(hu[left], hu[right], h[left], h[right], 0.5f*factor);
	Fhu[index] = LaxFriedrichFlux(hu[left]*hu[left]/h[left] + 0.5f*g*h[left]*h[left], 
								  hu[right]*hu[right]/h[right] + 0.5f*g*h[right]*h[right], 
								  hu[left], hu[right], 0.5f*dx/dt);
	Fhv[index] = LaxFriedrichFlux(hv[left]*hu[left]/h[left], hv[right]*hu[right]/h[right], hv[left], hv[right], 0.5f*dx/dt);
}



__global__ void compute_vertical_flux_kernel(float *Gh, float *Ghu, float *Ghv, float *h, float *hu, float *hv, int nx)
{
	int x = threadIdx.x + blockIdx.x*blockDim.x;
	int y = threadIdx.y + blockIdx.y*blockDim.y;

	int index = x + (nx+1)*y;
	int bottom = x + 1 + (nx + 2)*y;
	int top = x + 1 + (nx + 2)*(y + 1);

	float factor = fmaxf(fabs(hv[bottom]/h[bottom]), fabs(hv[top]/h[top]));
	Gh[index] = LaxFriedrichFlux(hv[bottom], hv[top], h[bottom], h[top], 0.5f*factor);
	Ghu[index] = LaxFriedrichFlux(hu[bottom]*hv[bottom]/h[bottom], hu[top]*hv[top]/h[top], hu[bottom], hu[top], 0.5f*dy/dt);
	Ghv[index] = LaxFriedrichFlux(hv[bottom]*hv[bottom]/h[bottom] + 0.5f*g*h[bottom]*h[bottom], 
		 						  hv[top]*hv[top]/h[top] + 0.5f*g*h[top]*h[top], 
		 						  hv[bottom], hv[top], 0.5f*dy/dt);
}



__global__ void compute_horizontal_edge_flux_kernel(float *Fh, float *Fhu, float *Fhv, float *h, float *hu, float *hv, int nx)
{
	int x = nx;
	int y = threadIdx.y + blockIdx.y*blockDim.y;

	int index = x + (nx+1)*y;
	int left = x + (nx+2)*(y+1);
	int right = x + 1 + (nx+2)*(y+1); 

	float factor = fmaxf(fabs(hu[left]/h[left]), fabs(hu[right]/h[right]));
	Fh[index] = LaxFriedrichFlux(hu[left], hu[right], h[left], h[right], 0.5f*factor);
	Fhu[index] = LaxFriedrichFlux(hu[left]*hu[left]/h[left] + 0.5f*g*h[left]*h[left], 
								  hu[right]*hu[right]/h[right] + 0.5f*g*h[right]*h[right], 
								  hu[left], hu[right], 0.5f*dx/dt);
	Fhv[index] = LaxFriedrichFlux(hv[left]*hu[left]/h[left], hv[right]*hu[right]/h[right], hv[left], hv[right], 0.5f*dx/dt);
}



__global__ void compute_vertical_edge_flux_kernel(float *Gh, float *Ghu, float *Ghv, float *h, float *hu, float *hv, int nx)
{
	int x = threadIdx.x + blockIdx.x*blockDim.x;
	int y = nx;

	int index = x + (nx+1)*y;
	int bottom = x + 1 + (nx + 2)*y;
	int top = x + 1 + (nx + 2)*(y + 1);

	float factor = fmaxf(fabs(hv[bottom]/h[bottom]), fabs(hv[top]/h[top]));
	Gh[index] = LaxFriedrichFlux(hv[bottom], hv[top], h[bottom], h[top], 0.5f*factor);
	Ghu[index] = LaxFriedrichFlux(hu[bottom]*hv[bottom]/h[bottom], hu[top]*hv[top]/h[top], hu[bottom], hu[top], 0.5f*dy/dt);
	Ghv[index] = LaxFriedrichFlux(hv[bottom]*hv[bottom]/h[bottom] + 0.5f*g*h[bottom]*h[bottom], 
		 						  hv[top]*hv[top]/h[top] + 0.5f*g*h[top]*h[top], 
		 						  hv[bottom], hv[top], 0.5f*dy/dt);
}



__global__ void update_height_kernel(float *h, float *Fh, float *Gh, int nx)
{
	int x = threadIdx.x + blockIdx.x*blockDim.x;
	int y = threadIdx.y + blockIdx.y*blockDim.y;

	int left = x + (nx+1)*y;
	int right = x + 1 + (nx+1)*y;
	int bottom = x + (nx+1)*y;
	int top = x + (nx+1)*(y+1);
	int centre = x + 1 + (nx + 2)*(y + 1);

	h[centre] = h[centre] - 0.5f*dt/dx*(Fh[right] - Fh[left]) - 0.5f*dt/dy*(Gh[top] - Gh[bottom]);
}



__global__ void update_horizontal_momentum_kernel(float *hu, float *Fhu, float *Ghu, int nx)
{
	int x = threadIdx.x + blockIdx.x*blockDim.x;
	int y = threadIdx.y + blockIdx.y*blockDim.y;

	int left = x + (nx+1)*y;
	int right = x + 1 + (nx+1)*y;
	int bottom = x + (nx+1)*y;
	int top = x + (nx+1)*(y+1);
	int centre = x + 1 + (nx + 2)*(y + 1);

	hu[centre] = hu[centre] - 0.5f*dt/dx*(Fhu[right] - Fhu[left]) - 0.5f*dt/dy*(Ghu[top] - Ghu[bottom]);
}



__global__ void update_vertical_momentum_kernel(float *hv, float *Fhv, float *Ghv, int nx)
{
	int x = threadIdx.x + blockIdx.x*blockDim.x;
	int y = threadIdx.y + blockIdx.y*blockDim.y;

	int left = x + (nx+1)*y;
	int right = x + 1 + (nx+1)*y;
	int bottom = x + (nx+1)*y;
	int top = x + (nx+1)*(y+1);
	int centre = x + 1 + (nx + 2)*(y + 1);

	hv[centre] = hv[centre] - 0.5f*dt/dx*(Fhv[right] - Fhv[left]) - 0.5f*dt/dy*(Ghv[top] - Ghv[bottom]);
}



__global__ void compute_maximum_timestep_kernel(int *mutex, float *h, float *hu, float *hv, float *hmax, float *vmax, float *vc, int nx, int ny)
{
	// int index = threadIdx.x + blockIdx.x*blockDim.x;
	// int stride = blockDim.x*gridDim.x;

	// __shared__ float h_cache[blockSize];
	// __shared__ float v_cache[blockSize];

	// float h_max = h[index];
	// float v_max = fmaxf(__fdividef(hu[index], h[index]), __fdividef(hv[index], h[index]));
	// int offset = stride;
	// while(index + offset < (nx+2)*(ny+2)){
	// 	h_max = fmaxf(h_max, h[index + offset]);
	// 	v_max = fmaxf(v_max, __fdividef(hu[index + offset], h[index + offset]));
	// 	v_max = fmaxf(v_max, __fdividef(hv[index + offset], h[index + offset]));
	// 	offset += stride;
	// }

	// h_cache[threadIdx.x] = h_max;
	// v_cache[threadIdx.x] = v_max; 

	// __syncthreads();

	// assumes blockDim.x is a powers of 2
	// int i = blockDim.x/2;
	// while(i != 0){
	// 	if(threadIdx.x < i){
	// 		h_cache[threadIdx.x] = fmaxf(h_cache[threadIdx.x], h_cache[threadIdx.x + i]);
	// 		v_cache[threadIdx.x] = fmaxf(v_cache[threadIdx.x], v_cache[threadIdx.x + i]);
	// 	}
	// 	__syncthreads();
	// 	i /= 2;
	// }

	// if(threadIdx.x == 0){
	// 	while (atomicCAS(mutex, 0 ,1) != 0); // lock
	// 	*hmax = fmaxf(*hmax, h_cache[0]);
	// 	*vmax = fmaxf(*vmax, v_cache[0]);
	// 	*vc = __fsqrt_rd(g*(*hmax)) + *vmax;
	// 	atomicExch(mutex, 0); // unlock
	// }
}






__global__ void compute_horizontal_flux_kernel2(float *Fh, float *Fhu, float *Fhv, float *h, float *hu, float *hv, int nx, int ny)
{
// 	int x = threadIdx.x + blockIdx.x*blockDim.x;
// 	int index = x;

// 	while(index < (nx+1)*ny){
// 		int left = x + (nx+2);
// 		int right = x + 1 + (nx+2);

// 		float factor = fmaxf(fabs(hu[left]/h[left]), fabs(hu[right]/h[right]));
// 		Fh[index] = LaxFriedrichFlux(hu[left], hu[right], h[left], h[right], 0.5f*factor);
// 		Fhu[index] = LaxFriedrichFlux(hu[left]*hu[left]/h[left] + 0.5f*g*h[left]*h[left], 
// 								  hu[right]*hu[right]/h[right] + 0.5f*g*h[right]*h[right], 
// 								  hu[left], hu[right], 0.5f*dx/dt);
// 		Fhv[index] = LaxFriedrichFlux(hv[left]*hu[left]/h[left], hv[right]*hu[right]/h[right], hv[left], hv[right], 0.5f*dx/dt);

// 		x += nx+2;
// 		index += nx+1;
// 	}
}



__global__ void compute_maximum_timestep_kernel2(int *mutex, float *h, float *hu, float *hv, float *hmax, float *vmax, float *vc, int nx)
{
	// int x = threadIdx.x + blockIdx.x*blockDim.x;
	// int y = threadIdx.y + blockIdx.y*blockDim.y;
	// int index = x+1 + (nx+2)*(y+1);

	// __shared__ float h_cache[blockSize][blockSize];
	// __shared__ float v_cache[blockSize][blockSize];

	// h_cache[threadIdx.x][threadIdx.y] = h[index];
	// v_cache[threadIdx.x][threadIdx.y] = fmaxf(__fdividef(hu[index], h_cache[threadIdx.x][threadIdx.y]), __fdividef(hv[index], h_cache[threadIdx.x][threadIdx.y]));

	// __syncthreads();

	// // assumes blockDim.x and blockDim.y are powers of 2
	// int i = blockDim.x/2;
	// int j = blockDim.y/2;
	// while(i != 0 && j != 0){
	// 	if(threadIdx.x < i && threadIdx.y < j){
	// 		h_cache[threadIdx.x][threadIdx.y] = fmaxf(h_cache[threadIdx.x][threadIdx.y], h_cache[threadIdx.x + i][threadIdx.y]);
	// 		v_cache[threadIdx.x][threadIdx.y] = fmaxf(v_cache[threadIdx.x][threadIdx.y], v_cache[threadIdx.x + i][threadIdx.y]);

	// 		h_cache[threadIdx.x][threadIdx.y] = fmaxf(h_cache[threadIdx.x][threadIdx.y], h_cache[threadIdx.x][threadIdx.y + j]);
	// 		v_cache[threadIdx.x][threadIdx.y] = fmaxf(v_cache[threadIdx.x][threadIdx.y], v_cache[threadIdx.x][threadIdx.y + j]);

	// 		h_cache[threadIdx.x][threadIdx.y] = fmaxf(h_cache[threadIdx.x][threadIdx.y], h_cache[threadIdx.x + i][threadIdx.y + j]);
	// 		v_cache[threadIdx.x][threadIdx.y] = fmaxf(v_cache[threadIdx.x][threadIdx.y], v_cache[threadIdx.x + i][threadIdx.y + j]);
	// 	}
	// 	__syncthreads();
	// 	i /= 2;
	// 	j /= 2;
	// }

	// if(threadIdx.x == 0 && threadIdx.y == 0){
	// 	while (atomicCAS(mutex, 0 ,1) != 0); // lock
	// 	*hmax = fmaxf(*hmax, h_cache[0][0]);
	// 	*vmax = fmaxf(*vmax, v_cache[0][0]);
	// 	*vc = __fsqrt_rd(g*(*hmax)) + *vmax;
	// 	atomicExch(mutex, 0); // unlock
	// }
}